<?php
/**
 * @file
 * Adds some common elements used in WWUZen to the style guide generated by the Style Guide module.
 * See this page for API information: https://www.drupal.org/node/1869732
 */
 
 /**
 * Implements hook_init().
 *
 * Includes CSS so that floated elements don't mess up page.
 */
function wwu_styleguide_extension_init() {
	//CSS to prevent floating elements from messing up the sections below them
	drupal_add_css(drupal_get_path('module', 'wwu_styleguide_extension') . '/styleguide.css');
}
 
 /**
 * Implements hook_styleguide().
 *
 * Adds some common elements used in WWUZen to the style guide generated by the Style Guide module.
 */
 
 function wwu_styleguide_extension_styleguide() {
 	//Javascript to make accordions appear correctly
	drupal_add_library('system', 'ui.accordion');
	
 	$items = array(
 	/* HELPER CLASSES */
 		'float-left' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Element aligned left (.float-left)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left'))) . styleguide_paragraph(2),
 		),
 		'float-right' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Element aligned right (.float-right)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-right'))) . styleguide_paragraph(2),
 		),
 		'helper-width-25-percent' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Quarter width element (.helper-width-25-percent)'),
 			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'helper-width-25-percent')),
 		),
 		'helper-width-33-percent' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Third width element (.helper-width-33-percent)'),
 			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'helper-width-33-percent')),
 		),
 		'helper-width-50-percent' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Half width element (.helper-width-50-percent)'),
 			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'helper-width-50-percent')),
 		),
 		'helper-width-66-percent' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Two thirds width element (.helper-width-66-percent)'),
 			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'helper-width-66-percent')),
 		),
 		'helper-width-75-percent' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Three quarters width element (.helper-width-75-percent)'),
 			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'helper-width-75-percent')),
 		),
 		'helper-width-100-percent' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Full width element (.helper-width-100-percent)'),
 			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'helper-width-100-percent')),
 		),
 		'no-margin' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove margin from e.g. floated element (.helper-no-margin)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin'))) . styleguide_paragraph(2),
 		),
 		'no-left-margin' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove left margin (.helper-no-left-margin)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-right helper-no-left-margin'))) . styleguide_paragraph(2),
 		),
 		'no-right-margin' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove right margin (.helper-no-right-margin)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-right-margin'))) . styleguide_paragraph(2),
 		),
 		'no-top-margin' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove top margin (.helper-no-top-margin)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-top-margin'))) . styleguide_paragraph(2),
 		),
 		'no-bottom-margin' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove bottom margin (.helper-no-right-margin)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-bottom-margin'))) . styleguide_paragraph(2),
 		),
 		'thin-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Thin padding (.helper-thin-padding)'),
 			'content'=> '<p>Shown with margin removed.</p>' . theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thin-padding'))) . styleguide_paragraph(2),
 		),
 		'medium-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Medium padding (.helper-medium-padding)'),
 			'content'=> '<p>Shown with margin removed.</p>' . theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-medium-padding'))) . styleguide_paragraph(2),
 		),
 		'thick-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Thick padding (.helper-thick-padding)'),
 			'content'=> '<p>Shown with margin removed.</p>' . theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thick-padding'))) . styleguide_paragraph(2),
 		),
 		'no-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove padding (.helper-no-padding)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thick-padding helper-no-padding'))) . styleguide_paragraph(2),
 		),
 		'no-left-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove left padding (.helper-no-left-padding)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thick-padding helper-no-left-padding'))) . styleguide_paragraph(2),
 		),
 		'no-right-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove right padding (.helper-no-right-padding)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thick-padding helper-no-right-padding'))) . styleguide_paragraph(2),
 		),
 		'no-top-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove top padding (.helper-no-top-padding)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thick-padding helper-no-top-padding'))) . styleguide_paragraph(2),
 		),
 		'no-bottom-padding' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Remove bottom padding (.helper-no-bottom-padding)'),
 			'content'=> theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'float-left helper-no-margin helper-thick-padding helper-no-bottom-padding'))) . styleguide_paragraph(2),
 		),
 		'center-content' => array(
 			'group' => t('Helper Classes'),
 			'title' => t('Center content (.helper-center-content)'),
 			'content'=> '<p class="helper-center-content">' . theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'))) . '</p>',
 		),
 	/* LISTS */
 		/* Definition List */
 		'dl' => array(
 			'group' => t('Lists'),
 			'title' => t('Definition List'),
 			'content' => '<dl>' .
 							'<dt>' . styleguide_word(1) . '</dt>' .
 								'<dd>' . styleguide_sentence() . '</dd>' .
 							'<dt>' . styleguide_word(1) . '</dt>' .
 								'<dd>' . styleguide_sentence() . '</dd>' .
 							'</dl>',
 		),
 	/* MEDIA */
 		/* Image in figure element */
		'figure-image' => array(
			'group' => t('Media'),
			'title' => t('Image within a figure'),
			'content' => '<figure>' . theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'))) . '</figure>',
		),
		/* Image with caption */
		'caption-image' => array(
			'group' => t('Media'),
			'title' => t('Image with caption (img.caption)'),
			'theme' => 'image',
    		'variables' => array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'), 'attributes' => array('class' => 'caption')),
		),
	/* MENUS */
		/* Primary tabs: ul with .primary class */
 		'ul.primary' => array(
 			'group' => t('Menus'),
 			'title' => t('Primary Tabs (ul.primary)'),
    		'theme' => 'links',
    		'variables' => array('links' => styleguide_ul_links(), 'attributes' => array('class' => 'primary')),
 		),
 		/* Secondary tabs: ul with .secondary class */
 		'ul.secondary' => array(
 			'group' => t('Menus'),
 			'title' => t('Secondary Tabs (ul.secondary)'),
    		'theme' => 'links',
    		'variables' => array('links' => styleguide_ul_links(), 'attributes' => array('class' => 'secondary')),
 		),
 		/* Main menu */
 		'mainmenu' => array(
 			'group' => t('Menus'),
 			'title' => t('Main Menu'),
 			'content' => '
 				<nav id="main-menu" class="main-nav" aria-label="Main navigation" role="navigation">
					<div class="region region-navigation">
						<div id="block-system-main-menu" class="block block-system contextual-links-region block-menu first last odd" role="navigation">
							<ul class="menu">
								<li class="menu__item is-leaf first leaf">
									<a class="menu__link menu-link-icon" href="/general/">
										<span class="menu-link-text">Home</span>
    								</a>
								</li>
								<li class="menu__item"><a class="menu__link" href="/general/content/test-webform">'.styleguide_word(1).'</a></li>
                				<li class="menu__item"><a class="menu__link" href="/general/content/test-webform">'.styleguide_word(1).'</a></li>
                				<li class="menu__item"><a class="menu__link" href="/general/content/test-webform">'.styleguide_word(1).'</a></li>
                				<li class="menu__item is-leaf last leaf"><a class="menu__link" href="/general/content/test-webform">'.styleguide_word(1).'</a></li>
            				</ul>
        				</div>
    				</div>
				</nav>',
 		),
 		/* Accordion Menu */
 		'accordion-menu' => array(
 			'group' => t('Menus'),
 			'title' => t('Accordion Menu'),
 			'content' => '<div class="block-accordion-menu" style="width:25%;">
    				<div class="ui-accordion">
						<h3 class="ui-accordion-header ui-helper-reset ui-state-default">
							<span class="ui-accordion-header-icon ui-icon ui-icon-plus"></span>
    						<a class="accordion-link" href="#accordion-menu">'.t('Collapsed link with children').'</a>
						</h3>
						<div class="ui-accordion-content" style="display: none;"></div>
						
						<h3 class="ui-accordion-header ui-helper-reset ui-state-default">
							<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
							<a class="accordion-link" href="#accordion-menu">'.t('Expanded link with children').'</a>
						</h3>
						<div class="ui-accordion-content" style="display: block;">
							<ul class="menu">
        						<li><a href="#accordion-menu">'.t('Nested link').'</a></li>
    						</ul>
						</div>
						
						<h3 class="ui-accordion-header ui-helper-reset ui-state-default">
							<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
							<a class="active-trail accordion-link" href="#accordion-menu">'.t('Expanded link with active child').'</a>
						</h3>
						<div class="ui-accordion-content" style="display: block;">
							<ul class="menu">
        						<li>
            						<a class="accordion-link" href="#accordion-menu">'.t('Nested link with children').'</a>
            						<ul class="menu">
                						<li><a class="accordion-link" href="#accordion-menu">'.t('Doubly nested link').'</a></li>
            						</ul>
        						</li>
        						<li><a class="active" href="#accordion-menu">'.t('Active nested link').'</a></li>
        						<li><a href="#accordion-menu">'.t('Nested link').'</a></li>
    						</ul>
					</div>
					
					<h3 class="no-children ui-accordion-header ui-helper-reset ui-state-default">
						<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
    					<a class="accordion-link" href="#accordion-menu">'.t('Primary link').'</a>
					</h3>
					<div class="ui-accordion-content" style="display: none;"></div>
					
					<h3 class="no-children ui-accordion-header ui-helper-reset ui-state-default">
						<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
    					<a class="active accordion-link" href="#accordion-menu">'.t('Active link').'</a>
					</h3>
					<div class="ui-accordion-content" style="display: none;"></div>
					
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default">
						<span class="ui-accordion-header-icon ui-icon ui-icon-plus"></span>
    					<a class="active accordion-link" href="#accordion-menu">'.t('Active primary link with children').'</a>
					</h3>
					<div class="ui-accordion-content" style="display: none;"></div>
					
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default">
						<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
						<a class="active accordion-link" href="#accordion-menu">'.t('Active primary link with children').'</a>
					</h3>
					<div class="ui-accordion-content" style="display: block;">
						<ul class="menu">
        					<li><a href="#accordion-menu">'.t('Nested link').'</a></li>
    					</ul>
					</div>
				</div>
			</div>',
 		),
 		/* Faculty pages menu */
 		'faculty-menu' => array(
 			'group' => t('Menus'),
 			'title' => t('Faculty Page Menu'),
 			'content' => '<div class="view-faculty-pages-menu" style="width:25%;">
					<div class="view-content">
						<div class="item-list">
							<ul>
								<li class="first">
									<a href="#faculty-menu">'.t('Primary link with active child').'</a>
    								<div class="item-list">
        								<ul>
            								<li class="first">
            									<a href="#faculty-menu">'.t('Level 2 link with children').'</a>
            									<div class="item-list">
            										<ul>
            											<li class="first last">
            												<a href="#faculty-menu">'.t('Level 3').'</a>
            												<div class="item-list">
            													<ul><li class="first last"><a href="#faculty-menu">'.t('Level 4').'</a></li></ul>
            												</div>
            											</li>
            										</ul>
            									</div>
            								</li>
            								<li class="active last"><a href="#faculty-menu">'.t('Level 2 active').'</a></li>
        								</ul>
    								</div>
								</li>
                				<li><a href="#faculty-menu">'.t('Primary link').'</a></li>
                				<li class="active"><a href="#faculty-menu">'.t('Active primary link').'</a></li>
                				<li class="active last">
                					<a href="#faculty-menu">'.t('Active primary link with children').'</a>
                					<div class="item-list">
                						<ul><li class="first last"><a href="#faculty-menu">'.t('Level 2').'</a></li></ul>
                					</div>
                				</li>
							</ul>
						</div>
					</div>
				</div>',
 		),
 	/* SYSTEM */
 		/* Fields on user profile pages - use several icons */
  		'user-fields' => array (
 			'group' => t('System'),
 			'title' => t('User Profile Page Fields'),
 			'content' => '<div class="user-individual-profile"><main style="background:none;">
 				<div class="field-your-photo">'
    				. theme('image', array('path' => styleguide_image('vertical'), 'alt' => t('My image'), 'title' => t('My image'))) . 
				'</div>
				<p class="field-academic-title">Faculty</p>     
				<p class="field-phone-number">(360) 650-3000</p>
				<p class="field-e-mail-address"><a href="#user-fields">Contact person by email</a></p>
				<p class="field-year-started"><span>2009</span></p>
				<p class="field-office-hours">MWF 11:00-12:00</p>
				<p class="field-about">' . styleguide_paragraph(1) . '</p>
				<div class="location-container">
					<p class="field-tr-location">Location 1</p>
					<p class="field-tr-location">Location 2</p>
				</div>
 			</main></div>',
 		),
 		/* Political Science user profiles */
 		'user-fields-2' => array (
 			'group' => t('System'),
 			'title' => t('Political Science Profile Fields'),
 			'content' => '<div class="user-individual-profile"><main style="background:none;">
 				<div class="rect_user_field">
 					<div class="field-your-photo">'
    					. theme('image', array('path' => styleguide_image('vertical'), 'alt' => t('My image'), 'title' => t('My image'))) . 
					'</div>
					<p class="field-academic-title">Faculty</p>
					<p class="field-education">' . styleguide_sentence(2) . '</p>     
					<p class="field-phone-number">(360) 650-3000</p>
					<p class="field-e-mail-address"><a href="#user-fields">Contact person by email</a></p>
					<p class="field-location">'. styleguide_word(2) .'</p>
					<p class="field-office-hours">MWF 11:00-12:00</p>
					<p class="field-about">' . styleguide_paragraph(1) . '</p>
				</div>
				<div class="rect_user_sidebar">
					<h3>'. styleguide_word(2) . '</h3>
					<p>'. styleguide_sentence(1) . '</p>
				</div>
 			</main></div>',
 		),
 	/* TABLES */
 		/* Responsive table */
 		$items['responsive-table'] = array(
 			'group' => t('Tables'),
    		'title' => t('Responsive Table (table.responsive)'),
    		'theme' => 'table',
    		'variables' => array('header' => styleguide_header(), 'rows' => styleguide_rows(), 'attributes' => array('class' => array('responsive')), 'caption' => styleguide_word(3)),
  		),
  	/* TEXT */
  		/* Block title (.block-title) */
  		$items['block-title'] = array(
  			'group' => t('Text'),
  			'title' => t('Block Title (.block-title)'),
  			'content' => '<h2 class="block-title">' . styleguide_word(3) . '</h2>',
  		),
  		/* Diminutive type (.diminutive-type) */
  		$items['diminutive-type'] = array(
  			'group' => t('Text'),
  			'title' => t('Diminutive Type (.diminutive-type)'),
  			'content' => '<p class="diminutive-type">' . styleguide_word(3) . '</p>',
  		),
  		/* Underlined Title (.title-underline) */
  		$items['underlined-title'] = array(
  			'group' => t('Text'),
  			'title' => t('Underlined Title (.title-underline)'),
  			'content' => '<h2 class="title-underline">' . styleguide_word(3) . '</h2>',
  		),
  	/* VIEWS */
  		/* Views accordion */
  		$items['views-accordion'] = array(
  			'group' => t('Views'),
  			'title' => t('Views Accordion'),
  			'content' => '<div class="ui-accordion">
					<div class="">
						<div class="ui-accordion-header ui-state-active">
							<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
							<span class="field-content"><a href="#views-accordion">' . styleguide_sentence() . '</a></span>
						</div>
						<div class="ui-accordion-content ui-helper-reset ui-accordion-content-active" style="display: block;">
							<div class="field-content"><p>' . styleguide_paragraph(1) . '</p></div>
        				</div>
    				</div>
					<div class="">
						<div class="ui-accordion-header ui-state-default">
							<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
    						<span class="field-content"><a href="#views-accordion">' . styleguide_sentence() . '</a></span>
						</div>
						<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: none; height: 30px;" aria-labelledby="ui-accordion-1-header-1" role="tabpanel" aria-expanded="false" aria-hidden="true">
							<div class="field-content"><p>' . styleguide_sentence() . '</p></div>
    					</div>
					</div>
				</div>',
  		),
  		/* Responsive grid */
  		$items['responsive-grid'] = array(
  			'group' => t('Views'),
  			'title' => t('Responsive Grid Format'),
  			'content' =>
  				'<div class="views-responsive-grid">
  					<div class="views-row">
  						<div class="views-column">'. styleguide_paragraph(1) .'</div>
  						<div class="views-column">'. styleguide_paragraph(1) .'</div>
  					</div>
  					<div class="views-row">
  						<div class="views-column">'. styleguide_paragraph(1) .'</div>
  						<div class="views-column">'. styleguide_paragraph(1) .'</div>
  					</div>
  				</div>'
  		),
  		/* User listings */
  		$items['user-listing'] = array(
  			'group' => t('Views'),
  			'title' => t('User Listing (div.user-module)'),
  			'content' =>
  				'<div class="user-module">
  					<a href="#user-listing">' . theme('image', array('path' => styleguide_image('vertical'), 'alt' => t('My image'), 'title' => t('My image'))) . '</a>
					<h4 class="field-content"><a href="#user-listing">' . styleguide_word(2) . '</a></h4>
					<p>' . styleguide_word(2) . '</p>
					<div class="phone">
						<span class="contact-icon"></span>
    					<span class="contact-icon-value">(360) 650-3000</span>
    				</div>
    				<div class="email">
						<span class="contact-icon"></span>
    					<span class="contact-icon-value">
							<p class="field-e-mail-address"><a href="#user-listing">webhelp@wwu.edu</a></p>
						</span>
  					</div>
  					<div class="location">
  						<span class="contact-icon"></span>
  						<span class="contact-icon-value">OM 390</span>
  					</div>
  				</div>
  				<div class="user-module">
  					<a href="#user-listing">' . theme('image', array('path' => styleguide_image('horizontal'), 'alt' => t('My image'), 'title' => t('My image'))) . '</a>
					<h4 class="field-content"><a href="#user-listing">' . styleguide_word(2) . '</a></h4>
					<p>' . styleguide_word(2) . '</p>
					<div class="phone">
						<span class="contact-icon"></span>
    					<span class="contact-icon-value">(360) 650-3000</span>
    				</div>
    				<div class="email">
						<span class="contact-icon"></span>
    					<span class="contact-icon-value">
							<p class="field-e-mail-address"><a href="#user-listing">webhelp@wwu.edu</a></p>
						</span>
  					</div>
  					<div class="location">
  						<span class="contact-icon"></span>
  						<span class="contact-icon-value">OM 390</span>
  					</div>
  				</div>',
  		)
	);
	return $items;
 }
 
 /**
 * Implements hook_styleguide_alter().
 *
 * Change "Breadcrumb" section to actually display breadcrumb
 */
function wwu_styleguide_extension_styleguide_alter(&$items) {
	//Build ordered list of links with random text
	$list = '<ol class="breadcrumb">';
  	for ($i = 0; $i < 6; $i++) {
    	$list .= '<li><a href="#styleguide_breadcrumb">' . styleguide_word(1) . '</a></li>';
	}
  	$list .= '</ol>';
  	//Set new content and title for existing 'styleguide_breadcrumb' section
  	$items['styleguide_breadcrumb']['content'] = $list;
  	$items['styleguide_breadcrumb']['title'] = t('Breadcrumb (ol.breadcrumb)');
}

?>