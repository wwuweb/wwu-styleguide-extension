# README #

WWU Styleguide Extension

A helper module for the Drupal Styleguide Module (https://www.drupal.org/project/styleguide), which is a dependency. Adds several interface elements from WWUZen that are not included in the default style guide.

Setup: enable the Styleguide module and this module. Then the sites style guide can be found at Appearance > Style guide